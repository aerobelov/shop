from django.contrib import admin
from . import models

from .models import Lot
from .models import Bet
from .models import Profile




admin.site.register(Lot)
admin.site.register(Bet)
admin.site.register(Profile)

# Register your models here.
