# Generated by Django 2.0.3 on 2018-04-21 05:43

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sale', '0005_auto_20180415_0031'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='last_bet',
            field=models.DateTimeField(default=datetime.datetime(2018, 4, 21, 5, 43, 40, 434398, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='lot',
            name='ended',
            field=models.DateTimeField(default=datetime.datetime(2018, 5, 22, 5, 43, 40, 433396, tzinfo=utc), verbose_name='expiration date'),
        ),
        migrations.AlterField(
            model_name='lot',
            name='started',
            field=models.DateTimeField(default=datetime.datetime(2018, 4, 21, 5, 43, 40, 433396, tzinfo=utc), verbose_name='date was issued'),
        ),
        migrations.AlterField(
            model_name='lot',
            name='sum',
            field=models.IntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='lot',
            name='winner',
            field=models.CharField(default='None', max_length=200),
        ),
    ]
