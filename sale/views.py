from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from sale.forms import SignUpForm, BetForm
from django.contrib.auth.models import User
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.views import View
from django.utils import timezone
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from .models import Lot, Bet, Profile


class IndexView(LoginRequiredMixin, generic.ListView):
    login_url = '/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'sale/index.html'
    context_object_name = 'latest_lots_list'
    paginate_by = 12
    Lot.check_date()

    def get_queryset(self):
        list = Lot.objects.filter(status='OPEN').order_by('-started')
        return list

class MyBetsView(LoginRequiredMixin, generic.ListView):
    login_url =  '/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'sale/mybets.html'
    context_object_name = 'my_bets_list'
    paginate_by = 12

    def get_queryset(self):
        list = Bet.objects.filter(owner=self.request.user)
        return list


class BetsView(LoginRequiredMixin, generic.ListView):
    login_url = '/login/'
    redirect_field_name = 'redirect_to'
    template_name = 'sale/bets.html'
    context_object_name = 'all_bets'
    paginate_by = 12

    def get_queryset(self):
        return Bet.objects.order_by('-timestamp')

class DetailView(View):

    template_name = 'sale/detail.html'
    form_class = BetForm
    context_object_name = "lot_info"

    def get(self, request, pk):
        form = BetForm()
        lot = Lot.objects.get(id=pk)
        context = {'form': form, 'lot':lot.name, 'amount':lot.sum, 'winner':lot.winner}
        return render(request, self.template_name, context)

    def post(self, request, pk):
        form = BetForm(request.POST)
        lot = Lot.objects.get(id=pk)
        user = request.user
        print (user.profile.amount)
        if form.is_valid():
            currentbet = form.cleaned_data.get('amount')
            if (lot.can_bet(currentbet)):
                print("can bet")
                if user.profile.enough_money(currentbet):
                    print("enough")
                    if user.profile.time_passed(lot.last):
                        print("TIME passed")
                        bet =  form.save(commit=False)
                        bet.owner = user
                        bet.lot = lot
                        bet.lot.winner = user.username
                        bet.timestamp = timezone.now()
                        bet.lot.sum = currentbet
                        user.profile.amount -= currentbet
                        bet.save()
                        lot.save()
                        user.profile.save()
            return redirect ('sale:index')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx['lot'] = Lot.objects.all()
        print(ctx)
        return ctx

class ProfileView(generic.DetailView):

    model = Profile
    template_name = 'sale/myprofile.html'

    def get_object(self):
        return Profile.objects.get(user=self.request.user)

def LogoutView(request):
    logout(request)
    return redirect('sale:index')


class signup(generic.CreateView):

    template_name = 'signup.html'
    form_class = SignUpForm
    success_url = reverse_lazy('login')

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            psw = form.cleaned_data['password1']
            user = User.objects.create_user(username, email, psw)
            user.refresh_from_db()
            print('1')
            user.profile.amount = form.cleaned_data.get('amount')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
              # load the profile instance created by the signal
            print('2')

            user.save()



            login(request, user)
            return redirect('sale:index')
        else:
            return HttpResponse('Form is invalid')

    def get(self, request):
        form = SignUpForm()
        context = {'form': form}
        return render(request, self.template_name, context)
