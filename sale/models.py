from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, pre_init
from django.dispatch import receiver
from django.utils import timezone


class Lot(models.Model):
    STATUSES = (('OPEN', 'Lot is open'),('ACTIVE','Lot active'),('CLOSED', 'Lot is closed'),)
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    started = models.DateTimeField('date was issued', default=timezone.now)
    ended = models.DateTimeField('expiration date', default=timezone.now() + timezone.timedelta(days=31))
    status = models.CharField(max_length=100, choices=STATUSES, default='OPEN')
    winner = models.CharField(max_length=200, default='None')
    sum = models.IntegerField(default=1)
    last = models.DateTimeField('last bet', default=timezone.now)

    def check_date():
        q = Lot.objects.filter(status='OPEN')
        for item in q:
            delta = item.ended - timezone.now()
            if delta.total_seconds() < 0:
                item.status = 'CLOSED'
                item.save()

    def can_bet(self, betsum):
        return (betsum > self.sum)

    def __str__(self):
        return self.name
	
class Bet(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    lot = models.ForeignKey(Lot, on_delete=models.CASCADE)
    timestamp = models.DateTimeField('date was issued')
    amount = models.IntegerField()

    def __str__(self):
        return self.lot.description


class Profile(models.Model):
    user = models.OneToOneField(
		settings.AUTH_USER_MODEL,
		related_name="profile",
		verbose_name=("user"),
		on_delete=models.CASCADE,
	)
    amount = models.IntegerField(null=True, blank=True, default=100)
    last_bet = models.DateTimeField(default=timezone.now)

    def time_passed(self, time):
        delta = self.last_bet - time
        print(delta)
        return ( delta.total_seconds() > 30 )

    def enough_money(self, sum):
        return self.amount > sum

    #def __str__(self):
     #   return self.user.name


@receiver(post_save, sender=get_user_model())
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
        instance.profile.save()
        print("Saved")


