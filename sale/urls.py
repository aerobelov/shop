from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'sale'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    url(r'^page/(?P<page>\d+)/$', views.IndexView.as_view()),
    path('profile/', views.ProfileView.as_view(), name='user_profile'),
    path('logout/', views.LogoutView, name='logout'),
    path('bets/', views.BetsView.as_view(), name="bets"),
    path('mybets/', views.MyBetsView.as_view(), name='mybets')
]