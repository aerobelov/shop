from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from sale.models import Bet

class SignUpForm(UserCreationForm):
    amount = forms.IntegerField(help_text='Required. Enter initial amount')

    class Meta:
        model = User
        fields = ('username', 'email', 'amount', 'password1', 'password2', )

class BetForm(forms.ModelForm):

    class Meta:
        model = Bet
        fields = ('amount',)